package com.bingbon.basequickadapter;

import com.chad.library.adapter.base.BaseProviderMultiAdapter;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.chad.library.adapter.base.module.LoadMoreModule;

public abstract class ProviderMultiAdapterWrap<T> extends BaseProviderMultiAdapter<T> {

    private OnWrapLoadMoreListener listener;

    public ProviderMultiAdapterWrap() {
        super();
        if (this instanceof LoadMoreModule) {
            setLoadMoreView(new LoadMoreView());
        }
    }
    public void setEnableLoadMore(boolean enable) {
        super.getLoadMoreModule().setEnableLoadMore(enable);
    }

    public void setHeaderFooterEmpty(boolean isHeadAndEmpty, boolean isFootAndEmpty) {
        super.setHeaderWithEmptyEnable(isHeadAndEmpty);
        super.setFooterWithEmptyEnable(isFootAndEmpty);
    }

    public void setOnLoadMoreListener(OnWrapLoadMoreListener listener) {
        this.listener = listener;
        super.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                ProviderMultiAdapterWrap.this.listener.onLoadMore();
            }
        });
    }

    public void setLoadMoreView(LoadMoreView view) {
        super.getLoadMoreModule().setLoadMoreView(view);
    }

    public void loadMoreEnd() {
        super.getLoadMoreModule().loadMoreEnd();
    }

    public void loadMoreEnd(boolean gone) {
        super.getLoadMoreModule().loadMoreEnd(gone);
    }

    public void loadMoreComplete() {
        super.getLoadMoreModule().loadMoreComplete();
    }

    public void loadMoreFail() {
        super.getLoadMoreModule().loadMoreFail();
    }

    public void setPreLoadNumber(int number) {
        super.getLoadMoreModule().setPreLoadNumber(number);
    }

    public boolean isLoading() {
        return super.getLoadMoreModule().isLoading();
    }

    public void disableLoadMoreIfNotFullPage() {
        super.getLoadMoreModule().checkDisableLoadMoreIfNotFullPage();
    }

    public void closeLoadAnimation() {
        super.setAnimationEnable(false);
    }
}
