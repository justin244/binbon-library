package com.bingbon.basequickadapter;

import com.chad.library.adapter.base.BaseNodeAdapter;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.chad.library.adapter.base.module.LoadMoreModule;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class NodeAdapterWrap extends BaseNodeAdapter {

    public NodeAdapterWrap(@Nullable List<BaseNode> nodeList) {
        super(nodeList);
        if(this instanceof LoadMoreModule){
            setLoadMoreView(new LoadMoreView());
        }
    }

    private OnWrapLoadMoreListener listener;

    public void setEnableLoadMore(boolean enable) {
        super.getLoadMoreModule().setEnableLoadMore(enable);
    }

    public void setHeaderFooterEmpty(boolean isHeadAndEmpty, boolean isFootAndEmpty) {
        super.setHeaderWithEmptyEnable(isHeadAndEmpty);
        super.setFooterWithEmptyEnable(isFootAndEmpty);
    }

    public void setOnLoadMoreListener(OnWrapLoadMoreListener listener) {
        this.listener = listener;
        super.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                NodeAdapterWrap.this.listener.onLoadMore();
            }
        });
    }

    public void setLoadMoreView(LoadMoreView view) {
        super.getLoadMoreModule().setLoadMoreView(view);
    }

    public void loadMoreEnd() {
        super.getLoadMoreModule().loadMoreEnd();
    }

    public void loadMoreEnd(boolean gone) {
        super.getLoadMoreModule().loadMoreEnd(gone);
    }

    public void loadMoreComplete() {
        super.getLoadMoreModule().loadMoreComplete();
    }

    public void loadMoreFail() {
        super.getLoadMoreModule().loadMoreFail();
    }

    public void setPreLoadNumber(int number) {
        super.getLoadMoreModule().setPreLoadNumber(number);
    }

    public boolean isLoading() {
        return super.getLoadMoreModule().isLoading();
    }

    public void disableLoadMoreIfNotFullPage(){
        super.getLoadMoreModule().checkDisableLoadMoreIfNotFullPage();
    }

    public void closeLoadAnimation() {
        super.setAnimationEnable(false);
    }

}
